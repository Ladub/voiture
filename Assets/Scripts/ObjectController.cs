﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectController : MonoBehaviour
{
  public bool hololens = false;
  public bool marker = false;
  public bool scapula = false;

  public bool object_check = false;

  public Image panel;

  public void Update()
  {
    if(hololens && marker && scapula)
    {
        object_check = true;
        panel.color = Color.green;
    }
  }
}
