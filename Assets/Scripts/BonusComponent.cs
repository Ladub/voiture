﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusComponent : MonoBehaviour
{
  public enum Bonus
  {
    SpeedUp,
    HealthUp
  }

  public Bonus _bonus;
  public GameObject player;

  private void OnTriggerEnter(Collider other)
    {
      Destroy(this.gameObject);

      if(_bonus == Bonus.SpeedUp)
      {
        SpeedUp();
      }
      if (_bonus == Bonus.HealthUp)
      {
        HealthUp();
      }
  }

  public void Update()
  {
    transform.RotateAround(transform.position,Vector3.up, 20 * Time.deltaTime);
  }

  private void SpeedUp()
  {
    if(player.GetComponent<PlayerController>().cpt_speed < 5)
    {
      player.GetComponent<PlayerController>().cpt_speed += 1;
      player.GetComponent<PlayerController>().SetActiveSpeedUI();
    }
  }

  private void HealthUp()
  {
      player.GetComponent<PlayerController>().health += 5;
  }


}
