﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

  public End end;

  public void RestartGame()
  {
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  public void QuitGame()
  {
    Application.Quit();
  }

  private void Update()
  {
    if(Input.GetKeyDown(KeyCode.Escape))
    {
      end.GameEnd();
    }
  }
}
