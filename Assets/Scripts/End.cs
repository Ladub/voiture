﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class End : MonoBehaviour
{
  public GameObject panel_end;
  public bool end = false;
  public ObjectController objects;
  public Timer timer;

  public TextMeshProUGUI end_text;

  private void OnTriggerEnter(Collider other)
  {
    if((other.gameObject.name == "Player" && objects.object_check))
    {
      end_text.text = GameEnd();
    }
  }

  public string GameEnd()
  {
    end = true;
    panel_end.SetActive(true);
    string finish = "Bravo\n";
    finish += "Score : " + ((int)(100 - timer.targetTime)).ToString() + "s";
    return finish;
  }
}
