﻿using System.Collections;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    private AudioSource[] music;


    private void Awake()
    {
        music = GetComponents<AudioSource>();
    }
    void Start()
    {
        Play();
    }

    public void Play()
    {
        for (int i = 0; i < music.Length; i++)
        {
            music[i].Play();
        }
        
    }

    public void Stop()
    {
        for (int i = 0; i < music.Length; i++)
        {
            music[i].Stop();
        }
    }

}
