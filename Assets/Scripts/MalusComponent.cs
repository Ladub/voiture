﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MalusComponent : MonoBehaviour
{
  public enum Malus
  {
    SpeedDown,
    HealthDown
  }

  public Malus _bonus;
  public GameObject player;

  private void OnTriggerEnter(Collider other)
  {
    Destroy(this.gameObject);

    if (_bonus == Malus.SpeedDown)
    {
      SpeedDown();
      
    }
    if (_bonus == Malus.HealthDown)
    {
      HealthDown();
    }
  }

  private void SpeedDown()
  {
    if(player.GetComponent<PlayerController>().cpt_speed > 0)
    {
      player.GetComponent<PlayerController>().cpt_speed -= 1;
      player.GetComponent<PlayerController>().DesactiveSpeedUI();
    }
      
  }

  private void HealthDown()
  {
    player.GetComponent<PlayerController>().health -= 5;
  }
}
