﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int health;
    public List<float> speed_list = new List<float> { 20, 30, 40, 50, 60 };
    public End end;
    public TextMeshProUGUI speed_text;
    public StartGame startgame;
    public int cpt_speed = 0;
    public List<RawImage> images_speed;

    private float turnSpeed = 80.0f;
    private float horizontalInput;
    private float forwardInput;
    

    public List<GameObject> voitures;
    public GameObject voiture_en_cours;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      if(!end.end && startgame.startgame)
      {
        speed_text.text = speed_list[cpt_speed].ToString();
        horizontalInput = Input.GetAxis("Horizontal");
        forwardInput = Input.GetAxis("Vertical");

        //Moves the car forward based on vertical input
        transform.Translate(Vector3.forward * Time.deltaTime * speed_list[cpt_speed] * forwardInput);

        //Rotates the car based on horizontal input
        transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed * horizontalInput);
      }
    }

  public void SelectVoiture(GameObject voiture)
  {
    voiture_en_cours.SetActive(false);
    voiture_en_cours = voiture;
    voiture.SetActive(true);
  }

  public void SetActiveSpeedUI()
  {
    images_speed[cpt_speed].color = Color.green;
  }

  public void DesactiveSpeedUI()
  {
    images_speed[cpt_speed].color = Color.red;
  }
}
