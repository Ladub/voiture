﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
  public GameObject panel_start;
  public bool startgame = false;

  public void StartTheGame()
  {
    startgame = true;
    panel_start.SetActive(false);
  }
}
