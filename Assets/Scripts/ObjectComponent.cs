﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectComponent : MonoBehaviour
{
  public Object _object;
  public ObjectController controller;
  public Image ui;

  public enum Object
  {
    Hololens,
    Marker,
    Scapula
  }
  public void Update()
  {
    transform.RotateAround(transform.position, transform.up, 40 * Time.deltaTime);
  }

  private void OnTriggerEnter(Collider other)
  {
    Destroy(this.gameObject);

    if(_object == Object.Hololens)
    {
      controller.hololens = true;
      ui.color = Color.white;
    }

    if (_object == Object.Scapula)
    {
      controller.scapula = true;
      ui.color = Color.white;
    }

    if (_object == Object.Marker)
    {
      controller.marker = true;
      ui.color = Color.white;
    }
  }

}
