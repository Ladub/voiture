﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
  public float targetTime = 60.0f;
  public TextMeshProUGUI timer;

  public End end;

  private void Update()
  {
    if(!end.end && GetComponent<StartGame>().startgame)
    {
      targetTime -= Time.deltaTime;
      timer.text = ((int)targetTime).ToString();

      if (targetTime <= 0.0f)
      {
        end.end = true;
        end.GameEnd();
      }

    }
    

  }
}
